using System;
using System.Net;
using HashidsNet;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Npgsql;
using RantBoxAPICore.Filters;
using RantBoxAPICore.Models;

namespace RantBoxAPICore.Controllers
{
    [ServiceFilter(typeof(DatabaseEnabledAttribute))]
    [Route("api/[controller]")]
    [ApiController]
    public class PatternController : ControllerBase
    {
        private readonly IOptions<DatabaseConfiguration> _config;
        private readonly ILogger<PatternController> _logger;
        private readonly Hashids hashIds;

        public PatternController(IOptions<DatabaseConfiguration> config, ILogger<PatternController> logger)
        {
            _config = config;
            _logger = logger;
            this.hashIds = new Hashids(_config.Value.HashSalt);
        }
        
        [HttpGet("get/{hash}")]
        [ProducesResponseType(typeof(ResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<PatternModel> Get(string hash)
        {
            var conn = new NpgsqlConnection(_config.Value.ConnectionString);

            //Decode returns an empty array if the string could not be decoded, check for that
            int[] pattern_id_decode = this.hashIds.Decode(hash);
            int pattern_id;

            if(pattern_id_decode.Length > 0)
            {
                pattern_id = pattern_id_decode[0];
            }
            else
            {
                return NotFound();
            }

            try
            {
                conn.Open();

                var cmd = new NpgsqlCommand("SELECT * FROM rant_patterns WHERE pattern_id = @p", conn);
                try
                {
                    cmd.Parameters.AddWithValue("p", pattern_id);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var pattern = reader.GetString(1);
                                return new PatternModel(pattern);
                            }
                        }
                        else
                        {
                            return NotFound();
                        }
                    }
                }
                catch(Exception e)
                {
                    _logger.LogError(e, "Error fetching pattern from database", null);
                }
                finally
                {
                    cmd.Dispose();
                }
            }
            catch (Exception e)
            {
               _logger.LogError(e, "Error while establishing database connection", null);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpPost("save")]
        [ProducesResponseType(typeof(ResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<string> Save([FromBody] PatternModel pattern)
        {
            var conn = new NpgsqlConnection(_config.Value.ConnectionString);
            try
            {
                conn.Open();

                var cmd = new NpgsqlCommand("INSERT INTO rant_patterns VALUES (DEFAULT, @p) RETURNING pattern_id;", conn);
                try
                {
                    cmd.Parameters.AddWithValue("p", pattern.Pattern);
                    Object new_id = cmd.ExecuteScalar();
					
                    if (new_id != null)
                    {
                        return this.hashIds.Encode((Int32)new_id);
                    }
                    else
                    {
                        return StatusCode(StatusCodes.Status500InternalServerError);
                    }
					
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Error saving pattern to database", null);
                }
                finally
                {
                    cmd.Dispose();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error while establishing database connection", null);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return StatusCode(StatusCodes.Status500InternalServerError);

        }
    }
}