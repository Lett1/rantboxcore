﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Rant;
using RantBoxAPICore.Models;
using RantBoxAPICore.Services;

namespace RantBoxAPICore.Controllers
{
    
    
    [Route("api/[controller]")]
    [ApiController]
    public class RantController : ControllerBase
    {
        private readonly RantEngine _rantEngine;
        private readonly IOptions<RantboxConfiguration> _config;
        private readonly ILogger<RantController> _logger;
        
        public RantController(IRantEngineService rantEngineService, IOptions<RantboxConfiguration> config, ILogger<RantController> logger)
        {
            this._rantEngine = rantEngineService.Get();
            this._config = config;
            this._logger = logger;
        }
        
        [HttpPost]
        [ProducesResponseType(typeof(ResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<ResponseModel> Run([FromBody]QueryModel query)
        {
            long seed = 0;
            Dictionary<string, string> output = null;

            try
            {
                var pgm = RantProgram.CompileString(query.Pattern);
                // Run the program
                var rantOutput = _rantEngine.Do(pgm, _config.Value.MaxLength, _config.Value.PatternTimeout);
                
                seed = rantOutput.Seed;
                output = rantOutput.Where(x => x.Value != "").ToDictionary(x => x.Name, x => x.Value);
                return Ok(new ResponseModel("success", "", seed, output));

            }
            catch (RantCompilerException e)
            {
                return StatusCode(500, new ResponseModel("compiler-error", e.Message, seed, output));
            }
            catch (RantRuntimeException e)
            {
                
                return StatusCode(500, new ResponseModel("runtime-error", e.Message, seed, output));
            }
        }
    }
}