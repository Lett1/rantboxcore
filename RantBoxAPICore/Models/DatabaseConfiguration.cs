namespace RantBoxAPICore.Models
{
    public class DatabaseConfiguration
    {
        public string ConnectionString { get; set; }
        public string HashSalt { get; set; }
    }
}