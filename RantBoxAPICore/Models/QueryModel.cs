namespace RantBoxAPICore.Models
{
    public class QueryModel
    {
        public string Pattern { get; set; }

        public string IncludeHidden { get; set; }
    }
}