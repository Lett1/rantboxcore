namespace RantBoxAPICore.Models
{
    public class RantboxConfiguration
    {
        public string Dictionary { get; set; }
        public double PatternTimeout { get; set; }
        public int MaxLength { get; set; }
    }
}