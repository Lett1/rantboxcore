namespace RantBoxAPICore.Models
{
    public class PatternModel
    {
        public string Pattern { get; set; }

        public PatternModel(string pattern)
        {
            this.Pattern = pattern;
        }
    }
}