using System.Collections.Generic;

namespace RantBoxAPICore.Models
{
    public class ResponseModel
    {
         public ResponseModel(string statusType, string statusMessage, long seed, Dictionary<string, string> output)
                {
                    StatusType = statusType;
                    StatusMessage = statusMessage;
                    Seed = seed;
                    Output = output;
                }
        
                public string StatusType { get; set; }
        
                public string StatusMessage { get; set; }
        
                public long Seed { get; set; }
        
                public Dictionary<string, string> Output { get; set; }

    }
}