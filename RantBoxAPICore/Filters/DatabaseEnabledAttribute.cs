using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using RantBoxAPICore.Models;

namespace RantBoxAPICore.Filters
{
    public class DatabaseEnabledAttribute : IActionFilter
    {
        private readonly IOptions<DatabaseConfiguration> _config;

        public DatabaseEnabledAttribute(IOptions<DatabaseConfiguration> config)
        {
            _config = config;
        }


        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (string.IsNullOrEmpty(this._config.Value.ConnectionString))
            {
                context.Result = new NotFoundResult();
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            
        }
    }
}