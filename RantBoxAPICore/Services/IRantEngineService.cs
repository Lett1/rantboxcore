using Rant;

namespace RantBoxAPICore.Services
{
    public interface IRantEngineService
    {
        RantEngine Get();
    }
}