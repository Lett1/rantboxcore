using System;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Rant;
using Rant.Vocabulary;
using RantBoxAPICore.Models;

namespace RantBoxAPICore.Services
{
    public class RantEngineService : IRantEngineService
    {
        private readonly IOptions<RantboxConfiguration> _config;
        private readonly ILogger<RantEngineService> _logger;

        public RantEngineService(IOptions<RantboxConfiguration> config, ILogger<RantEngineService> logger)
        {
            this._config = config;
            this._logger = logger;
        }

        public RantEngine Get()
        {
            var rant = new RantEngine();
            try
            {
                rant.LoadPackage(this._config.Value.Dictionary);
            }
            catch (Exception e)
            {
                this._logger.LogError(e, "Failed to load Dictionary package", this._config.Value.Dictionary);
            }

            return rant;
        }
    }
}